-- schema.sql
-- Giovanni Kraushaar
-- mail [at] giovannikraushaar.ch
-- 2019-12-11


/* ===========================
          TI ELECTIONS
         Database Schema
   =========================== */


/* Codestyle --------------------------------------------------------------- */

/*
* Table names start with upper-case letter followed by lower-case
* Attributes are always lower-case except "ID_"
* Underscore_separated names, NO CamelCase, whenever possible
* Constraints names all in capital letters with no underscore
* Use singular for names whenever possible
*/


/* Cleanup ----------------------------------------------------------------- */

-- Delete tables with the same name of those we are going to insert, if 
-- they exist (overwriting protection)
DROP TABLE IF EXISTS Spoiled ;
DROP TABLE IF EXISTS Election_municipality ;
DROP TABLE IF EXISTS Ballot ;
DROP TABLE IF EXISTS Candidate_municipality ;
DROP TABLE IF EXISTS Candidate_panachage ;
DROP TABLE IF EXISTS Candidate ;
DROP TABLE IF EXISTS Alias_candidate ;
DROP TABLE IF EXISTS Politician ;
DROP TABLE IF EXISTS Party ;
DROP TABLE IF EXISTS List_party ;
DROP TABLE IF EXISTS List_panachage ;
DROP TABLE IF EXISTS Alias_list ;
DROP TABLE IF EXISTS List ;
DROP TABLE IF EXISTS Municipality ;
DROP TABLE IF EXISTS District ;
DROP TABLE IF EXISTS Election ;
DROP TABLE IF EXISTS Year ;
DROP TABLE IF EXISTS Institution ;


/* Tables ------------------------------------------------------------------ */

CREATE TABLE Institution (
  ID_i        CHAR(2),
  institution VARCHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY (ID_i)
);

CREATE TABLE Year (
  year INT,
  PRIMARY KEY (year)
);

CREATE TABLE Election (
  ID_e     CHAR(6),
  year     INT,
  ID_i     CHAR(2),
  date     DATE,
  eligible INT,
  voter    INT,
  postal   INT,
  list_votes_tot INT,
  quotient FLOAT(8,2),
  seat     INT,
  PRIMARY KEY (ID_e),
  FOREIGN KEY (year) REFERENCES Year (year),
  FOREIGN KEY (ID_i) REFERENCES Institution (ID_i),
  UNIQUE (year,ID_i) 
);

CREATE TABLE District (
  ID_d            INT         AUTO_INCREMENT NOT NULL,
  name            VARCHAR(25) NOT NULL,
  bfs_district_id INT         NOT NULL,
  PRIMARY KEY (ID_d)
);

CREATE TABLE Municipality (
  ID_m   INT         AUTO_INCREMENT,
  name   VARCHAR(30) NOT NULL,
  bfs_id INT         NOT NULL,
  ID_d   INT,
  PRIMARY KEY (ID_m),
  FOREIGN KEY (ID_d) REFERENCES District (ID_d)
);

CREATE TABLE List (
  ID_l   CHAR(8),
  ID_e   CHAR(6),
  name   INT,
  number CHAR(2) NOT NULL,
  ballot INT,
  votes_issued         INT,
  votes_not_issued     INT,
  list_votes           INT,
  blank_votes          INT,
  seats                INT,
  seats_distribution_1 INT,
  seats_distribution_2 INT,       
  PRIMARY KEY (ID_l),
  FOREIGN KEY (ID_e) REFERENCES Election (ID_e)
  	ON UPDATE CASCADE  ON DELETE CASCADE,
  UNIQUE(ID_e,number)
);

CREATE TABLE Alias_list (
  ID_l  CHAR(8),
  alias VARCHAR(30),
  PRIMARY KEY (ID_l,alias),
  FOREIGN KEY (ID_l) REFERENCES List (ID_l)
  	ON UPDATE CASCADE  ON DELETE CASCADE
);

CREATE TABLE List_panachage (
  from_list CHAR(8),
  to_list   CHAR(8),
  vote      INT,
  PRIMARY KEY (from_list,to_list),
  FOREIGN KEY (from_list) REFERENCES List (ID_l),
  FOREIGN KEY (to_list)   REFERENCES List (ID_l)
);


-- In some cases it is difficult to tell whether it was a party-list or a joint-list. It a manual process that requires manual work and research on old newspaper. It can be postoned.
-- CREATE TABLE Party (
--   ID_party INT AUTO_INCREMENT,
--   name     VARCHAR(40) NOT NULL UNIQUE,
--   PRIMARY KEY (ID_party)
-- );

-- CREATE TABLE List_Party (
--   ID_l     CHAR(8),
--   ID_party INT,
--   PRIMARY KEY (ID_l,ID_party),
--   FOREIGN KEY (ID_l)     REFERENCES List (ID_l),
--   FOREIGN KEY (ID_party) REFERENCES Party (ID_party)
-- );

CREATE TABLE Politician (
  ID_p       INT         AUTO_INCREMENT,
  name       VARCHAR(50) NOT NULL,
  first_name VARCHAR(20) NOT NULL,
  last_name  VARCHAR(30) NOT NULL,
  birth      DATE,
  gender     CHAR(1),
  PRIMARY KEY (ID_p)
);

CREATE TABLE Alias_candidate (
  ID_a  INT AUTO_INCREMENT,
  ID_p  INT,
  alias VARCHAR(50),
  PRIMARY KEY (ID_a),
  FOREIGN KEY (ID_p) REFERENCES Politician (ID_p)
);

CREATE TABLE Candidate (
  ID_c              CHAR(10),
  ID_l              CHAR(8),
  ID_a              INT,
  number            CHAR(2) NOT NULL,
  elected           INT     NOT NULL,
  vote_personal     INT     NOT NULL,
  vote_list         INT,
  vote_preferencial INT,
  vote_external     INT,
  PRIMARY KEY (ID_c),
  FOREIGN KEY (ID_l) REFERENCES List (ID_l)
  	ON UPDATE CASCADE  ON DELETE CASCADE
  FOREIGN KEY (ID_a) REFERENCES Alias (ID_a)
  	ON UPDATE CASCADE  ON DELETE CASCADE
  UNIQUE(ID_l,ID_a)
);

CREATE TABLE Candidate_panachage (
  ID_c         CHAR(10),
  ID_l         INT,
  vote_outlist INT,
  PRIMARY KEY (ID_c,ID_l),
  FOREIGN KEY (ID_c) REFERENCES Candidate (ID_c)
  	ON UPDATE CASCADE  ON DELETE CASCADE,
  FOREIGN KEY (ID_l) REFERENCES List (ID_l)
  	ON UPDATE CASCADE  ON DELETE CASCADE
);

CREATE TABLE Candidate_municipality (
  ID_c  CHAR(10),
  ID_m  INT,
  vote  INT,
  PRIMARY KEY (ID_c,ID_m),
  FOREIGN KEY (ID_c) REFERENCES Candidate (ID_c)
  	ON UPDATE CASCADE  ON DELETE CASCADE,
  FOREIGN KEY (ID_m) REFERENCES Municipality (ID_m)
  	ON UPDATE CASCADE  ON DELETE CASCADE
);

CREATE TABLE Ballot (
  ID_e  CHAR(6),
  ID_l  CHAR(8),
  valid INT,
  sharp INT,
  has_preferencial     INT,
  inlist_preferencial  INT,
  outlist_preferencial INT,
  mixed_preferencial   INT,
  PRIMARY KEY (ID_e,ID_l),
  FOREIGN KEY (ID_e) REFERENCES Election (ID_e)
  	ON UPDATE CASCADE  ON DELETE CASCADE,
  FOREIGN KEY (ID_l) REFERENCES List (ID_l)
  	ON UPDATE CASCADE  ON DELETE CASCADE
);

CREATE TABLE Election_municipality (
  ID_e     CHAR(6),
  ID_m     INT,
  eligible INT,
  voter    INT,
  blank    INT,
  contest_municipality INT,
  contest_counting     INT,
  PRIMARY KEY (ID_e,ID_m),
  FOREIGN KEY (ID_e) REFERENCES Election (ID_e)
  	ON UPDATE CASCADE  ON DELETE CASCADE,
  FOREIGN KEY (ID_m) REFERENCES Municipality (ID_m)
  	ON UPDATE CASCADE  ON DELETE CASCADE
);

-- CREATE TABLE Spoiled (
--   ID_e     CHAR(6),
--   ID_m     INT,
--   PRIMARY KEY (ID_e,ID_m),
--   FOREIGN KEY (ID_e) REFERENCES Election (ID_e)
--   	ON UPDATE CASCADE  ON DELETE CASCADE,
--   FOREIGN KEY (ID_m) REFERENCES Municipality (ID_m)
--   	ON UPDATE CASCADE  ON DELETE CASCADE
-- );


/* Data -------------------------------------------------------------------- */

INSERT INTO Institution VALUES 
('CS', 'Consiglio di Stato'),
('GC', 'Gran Consiglio') ;
