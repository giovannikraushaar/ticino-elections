## Keys

* `year`: **Year**, format is `YYYY`. Available years are 2011, 2015 and 2019.
* `ID_i`: **Institution's ID**, either `CS` or `GC` which stand for *Consiglio di Stato* (governament) and *Gran Consiglio* (parliament) respectively.
* `ID_e`: **Election's ID**, format is `YYYYII` where `YYYY` is the year and `II` the institution the election refers to.
* `ID_l`: **List's ID**, format is `YYYYIILL` where `YYYY` is the year, `II` the institution and `LL` the number of the list during that session.
* `ID_p`: **Politician's ID**, arbitrary integer that refers to one person.
* `ID_c`: **Candidate's ID**. Idetifies a specific candidate in a specific election. Format is `YYYYIILLCC` where `YYYY` is the year, `II` the institution, `LL` the list of the candidate during that session and `CC` the number of the candidate inside the list.
* `ID_m`: **Municipality's ID**. arbitrary integer that identifies a municipality.
* `ID_a`: **Alias' ID**, arbitrary integer that refers to the alias of a politician.
* `ID_d`: **District's ID**, arbitrary integer that refers to a district in the District table.

## Tables

### Election
List all the election for which data is available, along with some aggregate figure on each of them. Elections taking place the same day, but concerning different institutions are considred as two distinct elements.

|Column|Description|
|---|---|
|`date` | Polling Day |
|`eligible` |Amount of people who were eleigibleto vote|
|`voter`|Amount of people who actually voted|
|`postal`|Amount of people who voted by mail|
|`list_votes_tot`|Amount of votes distributed in total among all the lists|
|`quotient`|Amount of votes required to a list in order to be entitled of a seat during the first sharing turn|
|`seat`|Total amount of seats to be shared in the election|

For more information, please refer to [ths table](https://www3.ti.ch/elezioni/Cantonali2019/ViewRisPartito.php?File=000_00_1&ElTipo=CS) and [this one](https://www3.ti.ch/elezioni/Cantonali2019/ViewSeggi.php?File=000_00_1&ElTipo=CS).

### List
Each candidate must be enrolled in a list which can coincide with a party, a joint-list of multiple parties or a list of independents (politicians who are not affiliated with any party). Each voter can chose *up to* one list and vote multiple candidates, either within and outside the list he/she may have chosen. This table shows election results at list level. A special list is number *99*: *"scheda senza intestazione"*, meaning that the voter (either explicitly or implictly) did not opt for any list but exclusively voted for individual candidates.

|Column|Description|
|---|---|
|`name`|Actual name of the list on the ballot.|
|`number`|List's number. This is not an ID. It is a number given to each list to help the voters. Indeed this numbers are not unique as they are independent across different sessions.|
|`ballot`|Amount of ballots in which the list was chosen. (*"schede"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewRisPartito.php?File=000_00_1&ElTipo=CS))|
|`votes_issued`|Total amount of votes for all candidates belonging to that list. (*"voti emessi"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewRisPartito.php?File=000_00_1&ElTipo=CS))|
|`votes_not_issued`|Preferencial votes not expressed by the voters that therefore count as votes to the list but not to any particular candidate. (*"non emessi"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewRisPartito.php?File=000_00_1&ElTipo=CS))|
|`list_votes`|Total amount of votes collected by the list. Sum of `votes_issued` and `votes_not_issued`. (*"voti di lista"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewRisPartito.php?File=000_00_1&ElTipo=CS))|
|`blank_votes`|Only applies to list 99. Uncast preferencial votes by voters who choose no list that therefore neither go to any list (in similar situations if a list was chosen these votes would have count as `votes_not_issued`).|
|`seats`|Seats assigned to list (*"totale seggi attribuiti"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewSeggi.php?File=000_00_1&ElTipo=CS))|
|`seats_distribution_1`|Seats assigned to the list during the first sharing turn. A list gets as many seats as `floor(list_votes / quotient)`,  where `quotient` can be found in `Election` table. (*"seggi 1° ripartizione"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewSeggi.php?File=000_00_1&ElTipo=CS))|
|`seats_distribution_2`|Seats assigned to the list during the second sharing turn. In this turn each of the n lists with the highest value of `list_votes - seats_distribution_1 * quotient` gets one seat; where n is the amount of seats left vacant after the first distribution. Lists that did not get any seat from the first sharing are excluded from this procedure. (*"seggi 2° ripartizione"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewSeggi.php?File=000_00_1&ElTipo=CS))|


### Politician
Contains general data on politicians.

|Column|Description|
|---|---|
|`name`|Politician's full name.|
|`birth`|Politician's birthday.|
|`gender`|Politician's sex. Either `M` for *male* or `F` for *female*.|


### Candidate
Contains candidates's results. A candidate is a politician in a specific election. The same politician in two distinct elections count as two distinct candidates.

|Column|Description|
|---|---|
|`number`|Number of the candidate inside the list.|
|`elected`|Binary: whether the candidate was elected or not.|
|`vote_personal`|Total votes issued for the candidate. Sum of `vote_list`, `vote_preferencial` and `vote_external`. (*"totale voti"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewVotiPersonali.php?File=000_00_1&ElTipo=CS))|
|`vote_list`|Votes coming from the list: each candidate gets one vote for each voter who gave preference to the list he/she belongs to. This value should coincide with `ballot` value in `List` table. (*"voti dalla propria lista - base"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewVotiPersonali.php?File=000_00_1&ElTipo=CS))|
|`vote_preferencial`|Votes for the candidate issued by voters of the candidate's list. (*"voti dalla propria lista - pref."* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewVotiPersonali.php?File=000_00_1&ElTipo=CS))|
|`vote_external`|Votes for the candidate issued by voters of other lists.|


### Alias_candidate
This table links a candidate to a politician. A *bridge table* like this one is reqiuired as some candidates are registered along with a nickname or some change last name because of marriage / divorce. `alias` is therefore the name under which a politician appears in a list. Each politician has one or more aliases.

### District

|Column|Description|
|---|---|
|`name`|Name of the district|
|`bfs_district_id`|ID used by the [*Federal Statistical Office*](https://www.agvchapp.bfs.admin.ch/) (bfs) to identify a district.|


### Municipality
Index of all municipalities in the Canton at the moment of the elections.

|Column|Description|
|---|---|
|`name`|Name of the municipality|
|`bfs_id`|ID (or *number* as they call it) used by the [*Federal Statistical Office*](https://www.agvchapp.bfs.admin.ch/) (bfs) to identify the municipality.|
|`ID_d`| District's ID, with foreign key in the `District` table.|


### Ballot
Some statistics concerning ballots.

|Column|Description|
|---|---|
|`valid`|Total valid ballots, sum of `list_sharp` and `has_pref`. (*"schede valide"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewValidePartito.php?File=000_00_1&ElTipo=CS))|
|`list_shap`|Ballots in which the voter only expressed a preference for a list but none for any candidate. (*"schede invariate"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewValidePartito.php?File=000_00_1&ElTipo=CS), also called *lista secca*)|
|`inlist_pref`|Ballots in which the voter gave preferencial votes *only* to candidates belonging to the list he/she opted for. (*"schede variate - solo preferenze proprie"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewValidePartito.php?File=000_00_1&ElTipo=CS))|
|`outlist_pref`|Ballots in which the voter gave preferencial votes *only* to candidates outside the list he/she selected. (*"schede variate - solo preferenze altrui"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewValidePartito.php?File=000_00_1&ElTipo=CS))|
|`mixed_pref`|Ballots in which the voter gave preferencial votes *both* to candidates in the list he/she selected and outside it. (*"schede variate - preferenze proprie e altrui"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewValidePartito.php?File=000_00_1&ElTipo=CS))|
|`has_pref `|Count of ballots in which one or more preferencial votes for a candidate have been expressed. Sum of `inlist_pref`, `outlist_pref` and `mixed_pref`. (*"totale schede variate"* in the [source table](https://www3.ti.ch/elezioni/Cantonali2019/ViewValidePartito.php?File=000_00_1&ElTipo=CS))|

### Candidate_panachage
Votes given to a candidate from voters of a specific list. The sum of the votes for a candidate in this table should be equal to the sum of `vote_preferencial` and `vote_external` in the `Candidate` table.

### Candidate_municipality 
Total personal votes received by a *candidate* in a *municipality*. The sum along a candidate should be equal to the `vote_personal` value in `Candidate` table.


### List_panachage
This list keeps track of the amount of votes that those voters who seected list `from_list` assigned to candidates of list `to_list`. More information may be found in [one of the source tables](https://www3.ti.ch/elezioni/Cantonali2019/ViewPreferenzialiListe.php?File=000_00_1&ElTipo=CS).


### Election_municipality
Election results at municipality level.

|Column|Description|
|---|---|
|`eligible` |Amount of people who were eleigibleto vote|
|`voter`|Amount of people who actually voted|
|`postal`|Amount of people who voted by mail|
|`valid `|Count of valid ballots|
|`spoiled`|Count of spoiled ballots|
|`blank`|Count of empty ballots|
|`contest_commune`|Ballots contested by the municipality|
|`contest_counting`|Ballots contested while counting|



### Institution
Index all possible political institions subject to cantonal ballot. These are the parliamentary chamber *Gran Consiglio* and the executive power *Consiglio di Stato*.


## Notes on data

* Panachage data for GC at candidate level is available only in 2015.
* Panachage data for CS at candidate level is **not** available in 2011.
* Municiapalities in Ticino went trough several merges between 2011 and 2019. Do not expect an equal split of the data in tables like `Election_municipality`.