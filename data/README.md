## Dataset description

### m_res

* `SCHEDE` Voti attribuiti alla lista
* `VOTI EMESSI` Somma dei voti di base e dei voti preferenziali di tutti i candidati della medesima lista
* `VOTI DI LISTA` Somma dei voti emessi e dei voti non emessi determinante per la ripartizione dei seggi
* `% SCHEDE` Percentuale di schede ottenute dalla lista calcolata sul totale delle schede valide, compresa la scheda senza intestazione
* `VOTI NON EMESSI` Voti preferenziali che non sono attivati in favore del singolo candidato, ma che sono comunque attribuiti alla lista votata e che entrano quindi nel computo per l'attribuzione dei seggi
* `VOTI BIANCHI` Voti non espressi nelle schede senza intestazione.


### ballot

Refer to [dettaglio schede valide](https://www3.ti.ch/elezioni/Cantonali2011/ViewValidePartito.php?File=000_00_1&ElTipo=CS).

- `id_b` id of the election
- `id_l` id of the list
- `valid` **SCHEDE VALIDE** total valid ballots, sum of `list_sharp` and `has_pref`.
- `list_sharp` **SCHEDE INVARIATE** also called *lista secca*, ballots in which the voter only expressed a preference for a list but none for any candidate.
- `inlist_pref` **SCHEDE VARIATE: SOLO PREFERENZE PROPRIE** ballots in which the voter gave preferencial votes *only* to candidates belonging to the list he/she opted for.
- `outlist_pref` **SCHEDE VARIATE: SOLO PREFERENZE ALTRUI** ballots in which the voter gave preferencial votes *only* to candidates outside the list he/she selected.
- `mixed_pref` **SCHEDE VARIATE: PREFERENZE PROPRIE E ALTRUI** allots in which the voter gave preferencial votes *both* to candidates in the list he/she selected and outside it.
- `has_pref` **TOTALE SCHEDE VARIATE** count of ballots in which one or more preferencial votes for a candidate have been expressed. Sum of `inliist_pref`, `outlist_pref` and `mixed_pref`.


### election_municipality

Refer to the small table in the upper-right corner of any [municipality result](https://www3.ti.ch/elezioni/Cantonali2015/ViewRisPartito.php?File=501_00_4&ElTipo=CS) and to the table in the bottom left of the same page.

- `id_b` id of the election
- `name` name of the municipality
- `eligible` **iscritti in catalogo**, people having the right to vote.
- `voter` **votanti**, people who actually voted.
- `mail` **votanti per corrispondenza**, 
- `valid` **schede valide**, count of valid ballots
- `spoiled` **schede nulle**, count of spoiled ballots
- `blank`  **schede bianche**, count of empty ballots
- `contest_commune` **schede contestate comune**
- `contest_counting` **schede contestate spoglio**


### list_municipality

Refer to the main table of any [municipality result](https://www3.ti.ch/elezioni/Cantonali2015/ViewRisPartito.php?File=501_00_4&ElTipo=CS).

- `id_b` id of the election
- `id_l` id of the list
- `name` name of the municipality
- `ballot ` **SCHEDE**, ballots in which `id_l` list was selected.
- `votes_issued` **VOTI EMESSI**, somma dei voti di base e dei voti preferenziali di tutti i candidati della medesima lista.
- `votes_not_issued` **NON EMESSI**, voti preferenziali che non sono attivati in favore del singolo candidato, ma che sono comunque attribuiti alla lista votata e che entrano quindi nel computo per l'attribuzione dei seggi.
- `votes_of_list` **VOTI DI LISTA**, somma dei voti emessi e dei voti non emessi determinante per la ripartizione dei seggi.
- `blank_votes` **VOTI BIANCHI**,  unexpressed votes on ballots in which *scheda senza intestazione* was selected.


## Note

`list_alias.csv` was hand compiled, and maps different names a list can have in different tables.