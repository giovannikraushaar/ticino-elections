# Ticino Elections

Data concerning elections in the Canton of Ticino (Tessin), Switzerland, in years 2011-2019, for both government and parliament. Data collected from the Canton's official website and organised in a coherent relational database.

## Introduction
The republic and Canton of Ticino uses a form of **panachage electoral system**. A brief information concerning this kind of system can be found on [Wikipedia](https://en.wikipedia.org/wiki/Panachage). More detailed information can be found in the [cantonal law that regulates](https://m3.ti.ch/CAN/RLeggi/public/index.php/raccolta-leggi/legge/num/676) the election. Reduced to four sentences a the electoral system in Ticino works as follows:

> *Lists* are sort of coalitions, each *candidate* belong to a *list*. Voters can vote for both *lists* and *candidates*. Votes to a *list* determine how many seats are assigned to a *list*. Depending on the amount of seats avilable, the *n* candidates in a list who received more votes are elected.

A description of the tables can be found in [`database/README.md`](database/README.md).


## Get the data
Data is provided as a SQLite self-contained database. It can be found in `database/ti-election.db`. 

#### Export to other DBMS
Running the following command will dump the database to a `.sql` file, though it will have some of the DBMS idiosyncrasies. Nevertheless there are scripts on the internet that allow to convert SQLite dumps into MySQL or PostgreSQL dumps.

```
sqlite3 database/ti-election.db '.dump' > database/ti_election.sql
```

#### Export to CSV

Run next command from the project root directory in your shell to export data in *csv* format. This script will create a bunch of *csv files* in `database/csv/`. It is **highly recommended** to use files generated this way, **NOT** those located in `data/` as the latter ones are intermediate files needed for building the database.


```
mkdir database/csv
for table in $(sqlite3 -csv database/ti-election.db ".tables"); do
	sqlite3 -header -csv database/ti-election.db "SELECT * FROM $table;" > database/csv/$table.csv
done
```

#### Adding Geospatial data
Shape files at municipality level are provided by the [Swiss Federal Office of Topography](https://shop.swisstopo.admin.ch/en/products/landscape/boundaries3D).

## Sources

* [Cantonal Administration website](https://www4.ti.ch/generale/dirittipolitici/diritti-politici/)
* [Federal Statistical Office website](https://www.agvchapp.bfs.admin.ch/it/home)
* [GeoNames](https://www.geonames.org)


##### tags
 #Ticino #Tessin #elezioni #election #database #SQL #politics #Switzerland #Svizzera

